import { List, Datagrid, TextField, SimpleList } from "react-admin";
import { CustomUrlField } from "../layouts/CustomUrlField";
import { CustomEmailField } from "../layouts/CustomEmailField";
import { useMediaQuery } from "@material-ui/core";

export const UserList = (props) => {
  const isSmall = useMediaQuery((theme) => theme.breakpoints.down("sm"));
  return (
    <List {...props}>
      {isSmall ? (
        <SimpleList
          primaryText={(record) => record.name}
        />
      ) : (
        <Datagrid rowClick="edit">
          <TextField source="id" />
          <TextField source="name" />
          <TextField source="username" />
          <CustomEmailField source="email" />
          <TextField source="address.street" />
          <TextField source="phone" />
          <CustomUrlField source="website" />
          <TextField source="company.name" />
        </Datagrid>
      )}
    </List>
  );
};
