import { useRecordContext } from "react-admin";
import { makeStyles } from "@material-ui/core/styles";
import PersonIcon from "@material-ui/icons/Person";

const useStyles = makeStyles({
  name: {
    textDecoration: "none",
    color: "#2196F3"
  },
  icon: {
    width: "0.5em",
    height: "0.5em",
    paddingLeft: 2,
    color: "#2196F3",
  },
});

export const CustomTextFieldForUserName = ({ source }) => {
  const classes = useStyles();
  const record = useRecordContext();
  return (
    <>
      {record ? (
        <a className={classes.name} href={record[source]}>
          {record[source]}
          <PersonIcon className={classes.icon} />
        </a>
      ) : null}
    </>
  );
};
