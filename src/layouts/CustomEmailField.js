import { useRecordContext } from "react-admin";
import { makeStyles } from "@material-ui/core/styles";
import EmailIcon from "@material-ui/icons/Email";

const useStyles = makeStyles({
  email: {
    textDecoration: "none",
    color: "#2196F3"
  },
  icon: {
    width: "0.5em",
    height: "0.5em",
    paddingLeft: 2,
    color: "#2196F3",
  },
});

export const CustomEmailField = ({ source }) => {
  const classes = useStyles();
  const record = useRecordContext();
  return (
    <>
      {record ? (
        <a className={classes.email} href={record[source]}>
          {record[source]}
          <EmailIcon className={classes.icon} />
        </a>
      ) : null}
    </>
  );
};
