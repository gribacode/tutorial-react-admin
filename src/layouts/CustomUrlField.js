import { useRecordContext } from "react-admin";
import { makeStyles } from "@material-ui/core/styles";
import LaunchIcon from "@material-ui/icons/Launch";

const useStyles = makeStyles({
  link: {
    textDecoration: "none",
    color: "#2196F3"
  },
  icon: {
    width: "0.5em",
    height: "0.5em",
    paddingLeft: 2,
    color: "#2196F3",
  },
});

export const CustomUrlField = ({ source }) => {
  const classes = useStyles();
  const record = useRecordContext();
  return (
    <>
      {record ? (
        <a className={classes.link} href={record[source]}>
          {record[source]}
          <LaunchIcon className={classes.icon} />
        </a>
      ) : null}
    </>
  );
};
