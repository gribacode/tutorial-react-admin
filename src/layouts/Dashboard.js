import { Card, CardContent, CardHeader } from "@material-ui/core";

export const Dashboard = () => {
  return (
    <Card>
      <CardHeader title="Welcome to the administration" />
      <CardContent>Start page</CardContent>
    </Card>
  );
};
