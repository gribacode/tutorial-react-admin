export const authProvider = {
  login: ({ username }) => {
    localStorage.setItem("username", username);
    return Promise.resolve();
  },
  logout: () => {
    localStorage.removeItem("username");
    return Promise.resolve();
  },
  checkError: ({ status }) => {
    if (status === 401 || status === 403) {
      localStorage.removeItem("username")
      return Promise.reject();
    }
    return Promise.resolve();
  },
  checkAuth: () => {
    const result = localStorage.getItem("username");
    if (result) {
      return Promise.resolve();
    }
    return Promise.reject();
  },
    getPermissions: () => {
      return Promise.resolve();
    },
  
}